import asyncio

from echo_server_client_protocol import EchoServerClientProtocol

class Main:
    """
    example from https://docs.python.org/3.6/library/asyncio-protocol.html#tcp-echo-server-protocol
    """
    def main(self):
        loop = asyncio.get_event_loop()
        coro = loop.create_server(EchoServerClientProtocol, None, 8778)
        server = loop.run_until_complete(coro)

        print(f'Serving on {server.sockets[0].getsockname()}')
        try:
            loop.run_forever()
        except KeyboardInterrupt:
            print('Ctrl-C')

        server.close()
        loop.run_until_complete(server.wait_closed())
        loop.close()
