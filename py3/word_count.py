#!/usr/bin/env python3

import asyncio

class WordCountProtocol(asyncio.Protocol):
    def connection_made(self, transport):
        peername = transport.get_extra_info('peername')
        print(f'Accepted connection from {peername}')
        self.transport = transport

    def data_received(self, data):
        message = data.decode()
        print(f'Data received: {repr(message)}')

        w = len(message)
        print(f'Send: {repr(w)}')
        self.transport.write(str(w).encode())

        print('Close the client socket')
        self.transport.close()

def main():
    loop = asyncio.get_event_loop()
    coro = loop.create_server(WordCountProtocol, None, 8778)
    server = loop.run_until_complete(coro)

    print(f'Serving on {server.sockets[0].getsockname()}')
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        print('Ctrl-C')

    server.close()
    loop.run_until_complete(server.wait_closed())
    loop.close()

if '__main__' == __name__:
    main()
