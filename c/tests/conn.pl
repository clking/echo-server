#!/usr/bin/perl

use feature qw(current_sub);

use AE;
use AnyEvent::Handle;
use Data::Dumper;

main();
sub main {
    $hd = AnyEvent::Handle->new(
        connect => ['localhost', 7788],
        on_read => sub {
            print 'got ', $hd->rbuf, $/;
        },
        on_error => sub {
            print 'on_error: ' . Dumper \@_;
            exit;
        },
        on_eof => sub {
            print 'on_eof' . Dumper \@_;
            exit;
        },
    );

    $w = AE::timer 0, 0, sub {
        $w = AE::timer rand 5, 0, __SUB__;
        $t = int AE::now % 10;

        $hd->push_write("$$ $t\n");
    };

    AE::cv->recv;
}
