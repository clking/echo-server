#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include <fcntl.h>

#include <getopt.h>
#include <time.h>

int Client[FD_SETSIZE];
unsigned client_index = 1;
fd_set in_fds, out_fds;
char buf[FD_SETSIZE][1 + BUFSIZ];

void loop_clients(void (*)(int, int));
void add_to_fd_set(int, int);
void check_fd_read(int, int);
void check_fd_write(int, int);
void remove_client_fd(int, int);

int
main(int argc, char** argv) {
    int server_fd, nfds;
    struct sockaddr_in addr;
    short port;
    struct timeval *timeout;

    server_fd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (server_fd == -1) {
        perror("socket");
        return server_fd;
    }

    port = argc > 1 ? atoi(argv[1]) : 7788;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = 0;
    if (0 > bind(server_fd, (struct sockaddr*)&addr, sizeof(addr))) {
        perror("bind");
        return 1;
    }

    if (0 > listen(server_fd, 1024)) {
        perror("listen");
        return 2;
    }

    /* at the very first, we try blocking forever to facilitate prototyping
    timeout = NULL;
    */
    timeout = (struct timeval *)malloc(sizeof(struct timeval));
    timeout->tv_sec = 0;
    timeout->tv_usec = 1000; // 1 ms

    fcntl(server_fd, F_SETFL, O_NONBLOCK);
    nfds = 1 + server_fd;
    while (1) {
        int sel;

        FD_ZERO(&in_fds);
        FD_ZERO(&out_fds);

        FD_SET(server_fd, &in_fds);

        loop_clients(add_to_fd_set);

        sel = select(nfds, &in_fds, &out_fds, NULL, timeout);
        if (sel > 0) {
            if (FD_ISSET(server_fd, &in_fds)) {
                int client_fd;
                socklen_t len;
                struct sockaddr_in client;

                if (0 > (client_fd = accept(server_fd, (struct sockaddr*)&client, (socklen_t*)&len))) {
                    perror("accept");
                    return 3;
                }
                nfds = 1 + client_fd;

                char addr[INET_ADDRSTRLEN];
                inet_ntop(AF_INET, &client.sin_addr, addr, INET_ADDRSTRLEN);
                printf("accepting connection from %s:%d\n", addr, client.sin_port);

                Client[client_index] = client_fd;
                ++client_index;
            }

            loop_clients(check_fd_read);
            loop_clients(check_fd_write);
        }
    }

    printf("end\n");

    return 0;
}

void
loop_clients(void (*func)(int, int)) {
    for (int i = 0 ; i < FD_SETSIZE ; ++i) {
        if (Client[i]) {
            func(i, Client[i]);
        }
    }
}

void
add_to_fd_set(int index, int client_fd) {
    FD_SET(client_fd, &in_fds);
    FD_SET(client_fd, &out_fds);
}

/* char buf[FD_SETSIZE][BUFSIZ] */
void
check_fd_read(int index, int client_fd) {
    if (!FD_ISSET(client_fd, &in_fds))
        return;

    FD_CLR(client_fd, &in_fds);
    bzero(buf[client_fd], 1 + BUFSIZ);

    int bytes_read = recv(client_fd, buf[client_fd], BUFSIZ, 0);
    printf("reading %d bytes from client #%d\n", bytes_read, client_fd);
    if (1 > bytes_read) {
        if (-1 == close(client_fd)) {
            perror("close");
        }
        printf("closed client #%d\n", client_fd);
        remove_client_fd(index, client_fd);

        return;
    }

    printf("In: %s\n", buf[client_fd]);
}

void
check_fd_write(int index, int client_fd) {
    if (!FD_ISSET(client_fd, &out_fds))
        return;

    FD_CLR(client_fd, &out_fds);

    if (!buf[client_fd][0])
        return;

    send(client_fd, buf[client_fd], strlen(buf[client_fd]), 0);

    printf("Out: %s\n", buf[client_fd]);

    bzero(buf[client_fd], 1 + BUFSIZ);
}

void
remove_client_fd(int index, int client_fd) {
    if (!Client[index] || client_fd != Client[index]) {
        printf("client mismatch? [%d] = mine %d vs. %d\n", index, Client[index], client_fd);
        return;
    }

    printf("removing Client[%d] = #%d\n", index, client_fd);
    Client[index] = 0;

}
